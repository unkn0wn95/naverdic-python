## Python Naver Dictionary API

[Naver Dictionary](http://dic.naver.com/) Python API

```python

from naverdic import NaverDic

NaverDic.search("꿈")

# >>> ['(잠자는 동안의 정신 현상) dream', '(희망) dream', '(비유적)']

NaverDic.search("dream")
# >>>['(자면서 꾸는) 꿈', '(희망을 담은) 꿈', '망상', '[칵테일용어]칵테일(Cocktail), 드림']

```

---

### Support

- English-Korean

- Korean-English

---

### Development Environment

- python 3.x
- bs4
