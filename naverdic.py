import urllib.request, urllib.parse
from bs4 import BeautifulSoup


class NaverDic:

    @staticmethod
    def search(word):

        # use mobile dictionary page (for crawl simplicity)
        base_url = "http://m.endic.naver.com/search.nhn?"

        param = {
            "searchOption": "entryIdiom",
            "query": word,
        }

        response = urllib.request.urlopen(base_url + urllib.parse.urlencode(param))
        page = response.read()
        soup = BeautifulSoup(page, 'html.parser')

        dic = []

        for header in soup.find_all("div", {"class": "word_wrap"}):
            found_word = header.select(".h_word > .target > .target")
            if len(found_word) == 0:    # if not found
                continue

            found_word = found_word[0].text.strip().lower()                     # exact word
            word_wrapper = header.select(".h_word > .target")[0].text.strip()   # word prefix, suffix

            if found_word.startswith(word.lower()) and \
                    (word_wrapper[len(word):] == "" or word_wrapper[len(word):].isdigit()):

                descriptions = header.select(".desc_lst > li > .desc")
                if len(descriptions) == 0:
                    continue

                for description in descriptions:
                    dic.append(description.text)

        return dic